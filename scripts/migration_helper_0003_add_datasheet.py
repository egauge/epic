"""
Import datasheet files into media for for EPIC Parts.

You should only use this immediately after running migration 0003_add_datasheet
if you were using the existing file directory crawl to provide datasheet links.
"""

import os
import re
import argparse

import django

def run(directory):
    from django.core.files import File
    from epic.models import Part, Datasheet
    parts = Part.objects.all()
    files = [f for f in os.listdir(directory) if os.path.isfile(
        os.path.join(directory, f))]
    # We will want to compare part name without file extension
    file_base = {}
    for f in files:
        file_base[os.path.splitext(f)[0]] = f
    for part in parts:
        part_name = '{} {}'.format(part.mfg, part.mfg_pn)
        part_match = part_name
        while True:
            if part_match in file_base:
                file_name = file_base[part_match]
                # If a Datasheet exists that fits the old filename matching
                # convention, we associate that existing Datasheet.
                try:
                    datasheet = Datasheet.objects.get(name=part_match)
                except Datasheet.DoesNotExist:
                    datasheet = Datasheet.objects.create(name=part_match)
                    data = open(os.path.join(directory, file_name),
                                'rb')
                    django_file = File(data)
                    datasheet.ds_file.save(file_name, django_file, save=True)
                print '{} associated with datasheet {}.'.format(
                    part_name, file_name)
                part.datasheet = datasheet
                part.save()
                break
            # try after stripping off the stuff after the last dash
            # (e.g., -REEL7)
            m = re.match(r'(.*)[-/][^-/]+$', part_match)
            if not m:
                print '{} has no corresponding datasheet.'.format(
                    part_name)
                break
            part_match = m.group(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Attach Datasheet to Part(s) with behavior changed in '
        '0003_add_datasheet. Datasheet files are expected to match '
        'Part names as: "{mfg} {mfg_pn}" where mfg is the part mfg field, '
        'and mfg_pn is the part mfg_pn field. If there is not an immediate '
        'match, filenames will be tried by removing chunks of the name, after '
        'a "-", until there are no more pieces to be tried and there is no '
        'match. This is the same name matching scheme used before the '
        'Datasheet model was added to generate datasheet links.')
    parser.add_argument('datasheet_directory',
                        type=str, help='The absolute path of the directory '
                        'where your datasheets are currently stored.')
    parser.add_argument('settings_file',
                        type=str, help='The Django project settings '
                        'to use. Module format. Must be on PYTHONPATH!')

    args = parser.parse_args()

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', args.settings_file)
    django.setup()

    run(args.datasheet_directory)
