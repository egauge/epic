# Generated by Django 3.2.14 on 2022-07-25 04:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('epic', '0010_auto_20211108_1718'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='last_bom_mod_name',
            field=models.CharField(blank=True, default='', help_text='Name of entity which last modified this part.', max_length=127),
        ),
        migrations.AlterField(
            model_name='part',
            name='mfg',
            field=models.CharField(help_text='The name of the manufacturer of the part.', max_length=127, verbose_name='Manufacturer'),
        ),
        migrations.AlterField(
            model_name='part',
            name='mfg_pn',
            field=models.CharField(max_length=127, verbose_name="Manufacturer's Part #"),
        ),
        migrations.AlterField(
            model_name='part',
            name='val',
            field=models.CharField(blank=True, default='', help_text='The primary value of the part such as resistance for a resistor or capacitance for a capacitor.', max_length=127, verbose_name='Value'),
        ),
        migrations.AlterField(
            model_name='vendor',
            name='name',
            field=models.CharField(max_length=127, unique=True, verbose_name='Vendor Name'),
        ),
        migrations.AlterField(
            model_name='vendor_part',
            name='vendor_pn',
            field=models.CharField(max_length=127, verbose_name="Vendor's Part #"),
        ),
        migrations.AlterField(
            model_name='warehouse',
            name='name',
            field=models.CharField(help_text='The name of the warehouse.', max_length=127, unique=True),
        ),
    ]
